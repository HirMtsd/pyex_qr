#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" PyQRCodeを使用し、QRコードのPNGファイルを作成するサンプルプログラム。 """

__author__    = "@HirMtsd"
__copyright__ = "Copyright 2022, @HirMtsd"
__date__      = "2022/09/02"
__email__     = "hirmtsd@gmail.com"
__version__   = "0.1"
__status__    = "Development"

# PyQRCodeについては、以下を参照
# https://pythonhosted.org/PyQRCode/
# venvで仮想環境を作成し、以下のコマンドでインストールすることを推奨。
# pip install pyqrcode
# pip install pypng

# Windows環境で、venvを使用している場合、shebangのせいで、
# 「ModuleNotFoundError: No module named 'pyqrcode'」
# が表示される場合は、pyランチャを使用しないで「python qr_w.py」と起動する。
# https://www.python.org/dev/peps/pep-0397/

import pyqrcode

code1 = pyqrcode.create('http://dummy.com/', version=1, error='L')
code1.png('code1.png', scale=2)
code1.show
