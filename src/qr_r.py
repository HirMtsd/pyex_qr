#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" PyQRCodeを使用し、QRコードを読み込むサンプルプログラム。 """

__author__    = "@HirMtsd"
__copyright__ = "Copyright 2022, @HirMtsd"
__date__      = "2022/09/02"
__email__     = "hirmtsd@gmail.com"
__version__   = "0.1"
__status__    = "Development"

# 先にpr_w.pyを実行する。

# OpenCV については、以下を参照
# https://opencv.org/
# venvで仮想環境を作成し、以下のコマンドでインストールすることを推奨。
# pip install opencv_python

# Windows環境で、venvを使用している場合、shebangのせいで、
# 「ModuleNotFoundError: No module named 'cv2'」
# が表示される場合は、pyランチャを使用しないで「python qr_r.py」と起動する。
# https://www.python.org/dev/peps/pep-0397/

import cv2

img = cv2.imread('code1.png')
qr = cv2.QRCodeDetector()
data,bbox,binImg = qr.detectAndDecode(img)
print('data:', data)
print('bbox:', bbox)
cv2.imshow('Image', binImg)
